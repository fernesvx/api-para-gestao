package com.lab2.api.repository;

import com.lab2.api.models.Product;

import java.util.HashMap;

public class Banco {
    private HashMap<Integer, Product> banco = new HashMap<>();
    private int id = 1;
    public void add(Product product){
        banco.put(id, product);
        id++;
    }

    public void remove(int id){
        banco.remove(id);
    }

    public void update(int id, String description, double price, int quantity){
        banco.get(id).setDescription(description);
        banco.get(id).setPrice(price);
        banco.get(id).setQuantity(quantity);
    }

    public HashMap<Integer, Product> view(){
        return banco;
    }
}
