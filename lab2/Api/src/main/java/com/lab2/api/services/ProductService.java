package com.lab2.api.services;

import com.lab2.api.models.Product;
import com.lab2.api.repository.Banco;
import org.springframework.stereotype.Service;

import java.util.Map;

@Service
public class ProductService {
    private Banco banco = new Banco();

    public String createProduct(String name, String description, double price, int quantity){
        try {
            banco.add(new Product(name, description, price, quantity));
            return "Product Created: \nName: " + name + "\nDescription: " + description +
                    "\nPrice: " + price + "\nQuantity: " + quantity;
        }catch (Exception e){
            return "ERROR "+ e +": Product can't be created";
        }
    }

    public String deleteProduct(int id){
        try{
            banco.remove(id);
            return "Product Removed";
        }catch (Exception e){
            return "ERROR"+ e +": Product can't be removed";
        }
    }

    public String updateProduct(int id, String description, double price, int quantity){
        try {
            banco.update(id, description, price, quantity);
            return "Product Updated \nDescription: " + description +
                    "\nPrice: " + price + "\nQuantity:" + quantity;
        }catch (Exception e){
            return "ERROR"+ e +": Product can't be updated";
        }
    }

    public String readProducts(){
        try{
            StringBuilder stringBuilder = new StringBuilder();

            for(Map.Entry<Integer, Product> productEntry : banco.view().entrySet()) {
                int id = productEntry.getKey();
                Product product = productEntry.getValue();

                stringBuilder.append(id).append(" | ");
                stringBuilder.append(product.getName()).append(" | ");
                stringBuilder.append(product.getDescription()).append(" | ");
                stringBuilder.append(product.getPrice()).append(" | ");
                stringBuilder.append(product.getQuantity()).append("/ - - - - /");
            }
            return stringBuilder.toString();
        }catch (Exception e){
            return "ERROR"+ e +": The list of Products can't be read";
        }
    }

}
