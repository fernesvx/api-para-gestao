package com.lab2.api.controllers;

import com.lab2.api.services.ProductService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ProductController {
    private final ProductService productService;

    public ProductController(ProductService productService){
        this.productService = productService;
    }

    @GetMapping("/create")
    public String includeProduct(@RequestParam String name, @RequestParam String description, @RequestParam double price, @RequestParam int quantity){
        return productService.createProduct(name, description, price, quantity);
    }

    @GetMapping("/delete")
    public String removeProduct(@RequestParam int id){
        return productService.deleteProduct(id);
    }

    @GetMapping("/update")
    public String changeProduct(@RequestParam int id, @RequestParam String description, @RequestParam double price, @RequestParam int quantity){
        return productService.updateProduct(id, description, price, quantity);
    }

    @GetMapping("/view")
    public String showProducts(){
        return productService.readProducts();
    }

}
